<?php

class Employee
{
    public $id;
    public $firstName;
    public $lastName;
    public $birthDay;
    public $divisionId;
    public $documentNumber;
    public $middleName;
    public $salary;

    public function __construct($id, $firstName, $lastName, $birthDay, $divisionId, $documentNumber, $middleName, $salary)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->birthDay = $birthDay;
        $this->divisionId = $divisionId;
        $this->documentNumber = $documentNumber;
        $this->middleName = $middleName;
        $this->salary = $salary;
    }
}
