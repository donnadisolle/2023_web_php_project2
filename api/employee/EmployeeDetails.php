<?php

class EmployeeDetails extends Employee
{
    public $divisionName;
    public $departmentName;
    public $isManager;

    public function __construct($id, $firstName, $lastName, $birthDay, $divisionId, $documentNumber, $middleName, $salary, $divisionName, $departmentName, $isManager)
    {
        parent::__construct($id, $firstName, $lastName, $birthDay, $divisionId, $documentNumber, $middleName, $salary);

        $this->divisionName = $divisionName;
        $this->departmentName = $departmentName;
        $this->isManager = $isManager;
    }
}
