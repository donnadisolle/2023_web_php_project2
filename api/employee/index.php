<?php
include "employee_service.php";

$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
// paskutinis demuo url adrese: pvz. jeigu adresas yra http://localhost:3000/api/employee/1
// tai $id bus 1

$endPart = basename($path);

// GET naudojamas - istraukti duomenis visus, kelis arba viena
if ($_SERVER["REQUEST_METHOD"] == "GET" && isValid($endPart)) {
    handleGetSingleEmployeeRequest($endPart);
}

if ($_SERVER["REQUEST_METHOD"] == "GET" && $endPart == "list") {
    handleGetAllEmployeesRequest();
}

// POST naudojamas kai norime sukurti nauja irasa
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    handlePostRequest();
}

// PUT naudojamas, kai norime pakeisti duomenis apie darbuotoja
if ($_SERVER["REQUEST_METHOD"] == "PUT" && isValid($endPart)) {
    handlePutRequest($endPart);
}

// DELETE naudojamas, kai norime istrinti duomenis
if ($_SERVER["REQUEST_METHOD"] == "DELETE" && isValid($endPart)) {
    handleDeleteRequest();
}

function isValid($endPart)
{
    return $endPart && is_numeric($endPart);
}
