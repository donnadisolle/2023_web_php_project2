<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/api/db_connect.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/employee/Employee.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/employee/EmployeeDetails.php";

class EmployeeRepository
{
    // Gauna visus darbuotojus
    public function getEmployees()
    {
        $conn = connectDB();
        $result = $conn->query("SELECT * FROM employees ORDER By Id DESC");

        if ($result === false) {
            throw new Exception("Klaida: " . $conn->error);
        }

        $employees = [];
        while ($data = $result->fetch_assoc()) {
            $employees[] = new Employee(
                $data["Id"],
                $data["FirstName"],
                $data["LastName"],
                $data["BirthDay"],
                $data["DivisionId"],
                $data["DocumentNumber"],
                $data["MiddleName"],
                $data["Salary"]
            );
        }

        $conn->close();

        return $employees;
    }

    // Gauna darbuotoją pagal ID
    public function getEmployee($employeeId)
    {
        $conn = connectDB();
        $stmt = $conn->prepare("SELECT * FROM employees WHERE Id = ?");

        if (!$stmt) {
            throw new Exception("Klaida formuojant SQL užklausą: " . $conn->error);
        }

        $stmt->bind_param("i", $employeeId);
        $employee = null;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $data = $result->fetch_assoc();

            if ($data) {
                $employee = new Employee(
                    $data["Id"],
                    $data["FirstName"],
                    $data["LastName"],
                    $data["BirthDay"],
                    $data["DivisionId"],
                    $data["DocumentNumber"],
                    $data["MiddleName"],
                    $data["Salary"]
                );
            } else {
                throw new Exception("Darbuotojas su ID " . $employeeId . " nerastas.");
            }
        } else {
            throw new Exception("Klaida vykdant SQL užklausą: " . $stmt->error);
        }

        $stmt->close();
        $conn->close();

        return $employee;
    }

    // Gauna darbuotojo detales pagal ID
    public function getEmployeeDetailsById($employeeId)
    {
        $conn = connectDB();
        $stmt = $conn->prepare("SELECT
            e.Id,
            e.FirstName,
            e.LastName,
            e.BirthDay,
            e.Salary,
            e.DivisionId,
            e.DocumentNumber,
            e.MiddleName,
            d.DivisionName,
            dp.DepartmentName
        FROM employees e
            LEFT JOIN divisions d ON e.DivisionId = d.Id
            LEFT JOIN departments dp ON d.DepartmentId = dp.Id
        WHERE e.Id = ?");

        if (!$stmt) {
            throw new Exception("Klaida formuojant SQL užklausą: " . $conn->error);
        }

        $stmt->bind_param("i", $employeeId);

        $employeeDetails = null;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $data = $result->fetch_assoc();

            $stmt2 = $conn->prepare("SELECT DepartmentName FROM departments WHERE ManagerId = ?");
            $stmt2->bind_param("i", $employeeId);
            $stmt2->execute();
            $managerResult = $stmt2->get_result();

            $managedDepartments = null;
            $isManager = false;
            if ($managerResult->num_rows > 0) {
                $isManager = true;
                $managedDepartmentData = $managerResult->fetch_assoc();
                $managedDepartments = $managedDepartmentData["DepartmentName"];
            }

            $employeeDetails = new EmployeeDetails(
                $data["Id"],
                $data["FirstName"],
                $data["LastName"],
                $data["BirthDay"],
                $data["DivisionId"],
                $data["DocumentNumber"],
                $data["MiddleName"],
                $data["Salary"],
                $data["DivisionName"],
                $managedDepartments,
                $isManager
            );
        } else {
            throw new Exception("Klaida vykdant SQL užklausą: " . $stmt->error);
        }

        $stmt->close();
        $conn->close();

        return $employeeDetails;
    }

    // Įterpia naują darbuotoją
    public function insertEmployee($name, $surname, $dateBirth, $divId, $docNumber, $midName, $salary)
    {
        $conn = connectDB();
        // paruošiame mysql komandą
        $stmt = $conn->prepare("INSERT INTO employees (FirstName, LastName, BirthDay, DivisionId, DocumentNumber, MiddleName, Salary) VALUES (?, ?, ?, ?, ?, ?, ?);");

        // prijugiam/priskiriam reikšmes/parametrus
        $stmt->bind_param("sssissd", $name, $surname, $dateBirth, $divId, $docNumber, $midName, $salary);

        // paleisim komanda
        if ($stmt->execute()) {
            // išsaugokite pranešimą sesijoje
            $_SESSION['message'] = "Duomenys įterpti!";
            return true;
        } else {
            throw new Exception("Error: " . $stmt->error);
        }
    }

    // Atnaujina darbuotojo informaciją
    public function updateEmployee($id, $firstName, $lastName, $birthDay, $divisionId, $documentNumber, $middleName, $salary)
    {
        $conn = connectDB();

        $stmt = $conn->prepare("UPDATE employees SET FirstName = ?, LastName = ?, BirthDay = ?, DivisionId = ?, DocumentNumber = ?, MiddleName = ?, Salary = ? WHERE Id = ?");

        if (!$stmt) {
            throw new Exception("Klaida formuojant SQL užklausą: " . $conn->error);
        }

        $stmt->bind_param(
            "sssissdi",
            $firstName,
            $lastName,
            $birthDay,
            $divisionId,
            $documentNumber,
            $middleName,
            $salary,
            $id
        );

        if (!$stmt->execute()) {
            throw new Exception("Klaida vykdant SQL užklausą: " . $stmt->error);
        }

        $stmt->close();
        $conn->close();

        return true;
    }
}
