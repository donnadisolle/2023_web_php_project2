<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/employee/Employee.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/employee/EmployeeDetails.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/employee/EmployeeRepository.php";

function handleGetAllEmployeesRequest()
{
    // susikurio repozitorijos objekta
    $repository = new EmployeeRepository();

    // kvieciu repozitorijis metoda
    $result = $repository->getEmployees();

    // nustatomas header, pasakoma klientui, kad bus grazinamas json formatas
    header('Content-Type: application/json');

    http_response_code(200);
    echo json_encode($result);
}

function handleGetSingleEmployeeRequest($id)
{
    // susikurio repozitorijos objekta
    $repository = new EmployeeRepository();

    // kvieciu repozitorijos metoda
    $result = $repository->getEmployeeDetailsById($id);

    // nustatomas header, pasakoma klientui, kad bus grazinamas json formatas
    header('Content-Type: application/json');

    http_response_code(200);
    echo json_encode($result);
}

function handlePostRequest()
{
    $json = file_get_contents('php://input');
    $employee = json_decode($json);

    // susikurio repozitorijos objekta
    $repository = new EmployeeRepository();

    // kvieciu repozitorijos metoda
    $result = $repository->insertEmployee(
        $employee->firstName,
        $employee->lastName,
        $employee->birthDay,
        $employee->divisionId,
        $employee->documentNumber,
        $employee->middleName,
        $employee->salary
    );

    // nustatomas header, pasakoma klientui, kad bus grazinamas json formatas
    header('Content-Type: application/json');

    // Send a response back to the client
    if ($result) {
        http_response_code(201); // 201 Created
        echo json_encode(array('message' => 'Employee created successfully'));
    } else {
        http_response_code(500); // 500 Internal Server Error
        echo json_encode(array('message' => 'An error occurred while creating the employee'));
    }
}

function handlePutRequest($id)
{
    // Get the input data
    $inputData = json_decode(file_get_contents('php://input'), true);

    // Create an Employee object
    $employee = new Employee(
        $id,
        $inputData['firstName'],
        $inputData['lastName'],
        $inputData['birthDay'],
        $inputData['divisionId'],
        $inputData['documentNumber'],
        $inputData['middleName'],
        $inputData['salary']
    );

    // Update the employee in the database
    $repository = new EmployeeRepository();
    $result = $repository->updateEmployee($employee->Id, $employee->FirstName, $employee->LastName, $employee->BirthDay, $employee->DivisionId, $employee->DocumentNumber, $employee->MiddleName, $employee->Salary);

    // nustatomas header, pasakoma klientui, kad bus grazinamas json formatas
    header('Content-Type: application/json');

    // Send the appropriate HTTP response
    if ($result) {
        http_response_code(200);
        echo json_encode(['message' => 'Employee updated successfully']);
    } else {
        http_response_code(500);
        echo json_encode(['message' => 'An error occurred while updating the employee']);
    }
}

function handleDeleteRequest()
{
    // nustatomas header, pasakoma klientui, kad bus grazinamas json formatas
    header('Content-Type: application/json');

    echo json_encode(['message' => 'Not supported']);
    http_response_code(200);
}
