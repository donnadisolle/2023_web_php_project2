<?php

class Client
{
    public $id;
    public $firstName;
    public $lastName;
    public $email;
    public $address;
    public $birthDay;
    public $coeficient;

    public function __construct($id, $firstName, $lastName, $email, $address, $birthDay, $coeficient)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->address = $address;
        $this->birthDay = $birthDay;
        $this->coeficient = $coeficient;
    }
}
