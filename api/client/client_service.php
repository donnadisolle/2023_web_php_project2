<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/client/Client.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/client/ClientDetails.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/client/ClientRepository.php";

function handleGetAllClientsRequest()
{

    $repository = new ClientRepository();

    $result = $repository->getClients();

    header('Content-Type: application/json');

    http_response_code(200);
    echo json_encode($result);
}

function handleGetSingleClientRequest($id)
{

    $repository = new ClientRepository();

    $result = $repository->getClients($id);

    header('Content-Type: application/json');

    http_response_code(200);
    echo json_encode($result);
}

function handlePostRequest()
{
    $json = file_get_contents('php://input');
    $client = json_decode($json);

    $repository = new ClientRepository();

    $result = $repository->insertClient(
        $client->firstName,
        $client->lastName,
        $client->email,
        $client->address,
        $client->birthDay,
        $client->coeficient,

    );

    header('Content-Type: application/json');

    if ($result) {
        http_response_code(201);
        echo json_encode(array('message' => 'Client created successfully'));
    } else {
        http_response_code(500);
        echo json_encode(array('message' => 'An error occurred while creating the client'));
    }
}

function handlePutRequest($id)
{

    $inputData = json_decode(file_get_contents('php://input'), true);

    $client = new Client(
        $id,
        $inputData['firstName'],
        $inputData['lastName'],
        $inputData['email'],
        $inputData['address'],
        $inputData['birthDay'],
        $inputData['coeficient']
    );

    $repository = new ClientRepository();
    $result = $repository->updateClient($client->id, $client->firstName, $client->lastName, $client->email, $client->address, $client->birthDay, $client->coeficient);

    header('Content-Type: application/json');

    if ($result) {
        http_response_code(200);
        echo json_encode(['message' => 'Client updated successfully']);
    } else {
        http_response_code(500);
        echo json_encode(['message' => 'An error occurred while updating the client']);
    }
}

function handleDeleteRequest()
{

    header('Content-Type: application/json');

    echo json_encode(['message' => 'Not supported']);
    http_response_code(200);
}
