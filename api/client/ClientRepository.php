<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/api/db_connect.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/client/Client.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/client/ClientDetails.php";

class ClientRepository
{

    public function getClients()
    {
        $conn = connectDB();
        $result = $conn->query("SELECT * FROM clients ORDER By id DESC");

        if ($result === false) {
            throw new Exception("Klaida: " . $conn->error);
        }

        $clients = [];
        while ($data = $result->fetch_assoc()) {
            $clients[] = new Client(
                $data["id"],
                $data["vardas"],
                $data["pavarde"],
                $data["el_pastas"],
                $data["adresas"],
                $data["gimimo_data"],
                $data["koeficientas"],

            );
        }

        $conn->close();

        return $clients;
    }

    public function getClient($clientId)
    {
        $conn = connectDB();
        $stmt = $conn->prepare("SELECT * FROM clients WHERE id = ?");

        if (!$stmt) {
            throw new Exception("Klaida formuojant SQL užklausą: " . $conn->error);
        }

        $stmt->bind_param("i", $clientId);
        $client = null;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $data = $result->fetch_assoc();

            if ($data) {
                $client = new Client(
                    $data["id"],
                    $data["vardas"],
                    $data["pavarde"],
                    $data["el_pastas"],
                    $data["adresas"],
                    $data["gimimo_data"],
                    $data["koeficientas"],

                );
            } else {
                throw new Exception("Darbuotojas su ID " . $clientId . " nerastas.");
            }
        } else {
            throw new Exception("Klaida vykdant SQL užklausą: " . $stmt->error);
        }

        $stmt->close();
        $conn->close();

        return $client;
    }

    public function insertClient($name, $surname, $email, $address, $birthDay, $coeficient)
    {
        $conn = connectDB();

        $stmt = $conn->prepare("INSERT INTO clients (vardas, pavarde, el_pastas, adresas, gimimo_data, koeficientas) VALUES (?, ?, ?, ?, ?, ?);");

        $stmt->bind_param("sssssd", $name, $surname, $email, $address, $birthDay, $coeficient);

        if ($stmt->execute()) {

            $_SESSION['message'] = "Duomenys įterpti!";
            return true;
        } else {
            throw new Exception("Error: " . $stmt->error);
        }
    }

    public function updateClient($id, $firstName, $lastName, $email, $address, $birthDay, $coeficient)
    {
        $conn = connectDB();

        $stmt = $conn->prepare("UPDATE clients SET vardas = ?, pavarde = ?, el_pastas = ?, adresas = ?, gimimo_data = ?, koeficientas = ? WHERE id = ?");

        if (!$stmt) {
            throw new Exception("Klaida formuojant SQL užklausą: " . $conn->error);
        }

        $stmt->bind_param(
            "sssssdi",
            $firstName,
            $lastName,
            $email,
            $address,
            $birthDay,
            $coeficient,
            $id
        );

        if (!$stmt->execute()) {
            throw new Exception("Klaida vykdant SQL užklausą: " . $stmt->error);
        }

        $stmt->close();
        $conn->close();

        return true;
    }
}
