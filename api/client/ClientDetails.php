<?php

class ClientDetails extends Client
{
    public $divisionName;
    public $departmentName;
    public $isManager;

    public function __construct($id, $firstName, $lastName, $email, $address, $birthDay, $coeficient, $divisionName, $departmentName, $isManager)
    {
        parent::__construct($id, $firstName, $lastName, $email, $address, $birthDay, $coeficient);

        $this->divisionName = $divisionName;
        $this->departmentName = $departmentName;
        $this->isManager = $isManager;
    }
}
