<?php
include "client_service.php";

$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

$endPart = basename($path);

if ($_SERVER["REQUEST_METHOD"] == "GET" && isValid($endPart)) {
    handleGetSingleClientRequest($endPart);
}

if ($_SERVER["REQUEST_METHOD"] == "GET" && $endPart == "list") {
    handleGetAllClientsRequest();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    handlePostRequest();
}

if ($_SERVER["REQUEST_METHOD"] == "PUT" && isValid($endPart)) {
    handlePutRequest($endPart);
}

if ($_SERVER["REQUEST_METHOD"] == "DELETE" && isValid($endPart)) {
    handleDeleteRequest();
}

function isValid($endPart)
{
    return $endPart && is_numeric($endPart);
}
