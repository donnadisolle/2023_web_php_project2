<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/api/db_connect.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/department/Department.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/department/DepartmentDetails.php";

class DepartmentRepository
{

    public function getDepartments()
    {
        $conn = connectDB();
        $result = $conn->query("SELECT * FROM departments ORDER By Id DESC");

        if ($result === false) {
            throw new Exception("Klaida: " . $conn->error);
        }

        $departments = [];
        while ($data = $result->fetch_assoc()) {
            $departments[] = new Department(
                $data["Id"],
                $data["DepartmentName"],
                $data["ManagerId"],

            );
        }

        $conn->close();

        return $departments;
    }

    // Gauna darbuotojo detales pagal ID
    public function getDepartmentDetailsById($Id)
    {
        $conn = connectDB();
        $stmt = $conn->prepare("SELECT
             Id,
             DepartmentName,
             ManagerId

         FROM departments

         WHERE Id = ?");

        if (!$stmt) {
            throw new Exception("Klaida formuojant SQL užklausą: " . $conn->error);
        }

        $stmt->bind_param("i", $Id);

        $departmentDetails = null;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $data = $result->fetch_assoc();

            $stmt2 = $conn->prepare("SELECT DepartmentName FROM departments WHERE Id = ?");
            $stmt2->bind_param("i", $employeeId);
            $stmt2->execute();
            $managerResult = $stmt2->get_result();

            $departmentDetails = new DepartmentDetails(
                $data["Id"],
                $data["DepartmentName"],
                $data["ManagerId"]

            );
        } else {
            throw new Exception("Klaida vykdant SQL užklausą: " . $stmt->error);
        }

        $stmt->close();
        $conn->close();

        return $departmentDetails;
    }

    public function insertDepartment($Id, $DepartmentName, $ManagerId)
    {
        $conn = connectDB();
        // paruošiame mysql komandą
        $stmt = $conn->prepare("INSERT INTO departments (Id, DepartmentName, ManagerId) VALUES (?, ?, ?);");

        // prijugiam/priskiriam reikšmes/parametrus
        $stmt->bind_param("iss", $Id, $DepartmentName, $ManagerId);

        // paleisim komanda
        if ($stmt->execute()) {
            // išsaugokite pranešimą sesijoje
            $_SESSION['message'] = "Duomenys įterpti!";
            return true;
        } else {
            throw new Exception("Error: " . $stmt->error);
        }
    }

    // Atnaujina darbuotojo informaciją
    public function updateDepartment($Id, $DepartmentName, $ManagerId)
    {
        $conn = connectDB();

        $stmt = $conn->prepare("UPDATE departments SET DepartmentName = ?, ManagerId = ? WHERE Id = ?");
        if (!$stmt) {
            throw new Exception("Klaida formuojant SQL užklausą: " . $conn->error);
        }

        // Move the bind_param() call outside the if-block
        $stmt->bind_param("sii", $DepartmentName, $ManagerId, $Id);

        if (!$stmt->execute()) {
            throw new Exception("Klaida vykdant SQL užklausą: " . $stmt->error);
        }

        $stmt->close();
        $conn->close();

        return true;
    }
}
