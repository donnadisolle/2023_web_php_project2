<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/department/Department.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/department/DepartmentDetails.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/department/DepartmentRepository.php";

function handleGetAllDepartmentRequest()
{
    $repository = new DepartmentRepository();

    $result = $repository->getDepartments();

    header('Content-Type: application/json');

    http_response_code(200);
    echo json_encode($result);
}

function handleGetSingleDepartmentRequest($id)
{
    $repository = new DepartmentRepository();

    $result = $repository->getDepartmentDetailsById($id);

    header('Content-Type: application/json');

    http_response_code(200);
    echo json_encode($result);
}

function handlePostRequest()
{
    $json = file_get_contents('php://input');
    $department = json_decode($json);

    $repository = new DepartmentRepository();

    $result = $repository->insertDepartment(
        $department->Id,
        $department->DepartmentName,
        $department->ManagerId

    );

    header('Content-Type: application/json');

    if ($result) {
        http_response_code(201); // 201 Created
        echo json_encode(array('message' => 'Department created successfully'));
    } else {
        http_response_code(500); // 500 Internal Server Error
        echo json_encode(array('message' => 'An error occurred while creating the department'));
    }
}

function handlePutRequest($id)
{
    $inputData = json_decode(file_get_contents('php://input'), true);

    $department = new Department(
        $id,
        $inputData['DepartmentName'],
        $inputData['ManagerId']

    );

    $repository = new DepartmentRepository();
    $result = $repository->updateDepartment($department->id, $department->departmentName, $department->managerId);

    header('Content-Type: application/json');

    if ($result) {
        http_response_code(200);
        echo json_encode(['message' => 'Department updated successfully']);
    } else {
        http_response_code(500);
        echo json_encode(['message' => 'An error occurred while updating the department']);
    }
}

function handleDeleteRequest()
{
    header('Content-Type: application/json');

    echo json_encode(['message' => 'Not supported']);
    http_response_code(200);
}
