<?php

class Department
{
    public $id;
    public $departmentName;
    public $managerId;

    public function __construct($id, $departmentName, $managerId)
    {
        $this->id = $id;
        $this->departmentName = $departmentName;
        $this->managerId = $managerId;
    }
}
