<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/api/db_connect.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/division/Division.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/division/DivisionDetails.php";

class DivisionRepository
{
    // Gauna visus padalinius
    public function getDivisions()
    {
        $conn = connectDB();
        $result = $conn->query("SELECT * FROM divisions ORDER By Id DESC");

        if ($result === false) {
            throw new Exception("Klaida: " . $conn->error);
        }

        $divisions = [];
        while ($data = $result->fetch_assoc()) {
            $divisions[] = new Division(
                $data["Id"],
                $data["DivisionName"],
                $data["DepartmentId"]
            );
        }

        $conn->close();

        return $divisions;
    }

    // Gauna padalinį pagal ID
    public function getDivision($ivisionId)
    {
        $conn = connectDB();
        $stmt = $conn->prepare("SELECT * FROM divisions WHERE Id = ?");

        if (!$stmt) {
            throw new Exception("Klaida formuojant SQL užklausą: " . $conn->error);
        }

        $stmt->bind_param("i", $divisionId);
        $division = null;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $data = $result->fetch_assoc();

            if ($data) {
                $division = new Division(
                    $data["Id"],
                    $data["DivisionName"],
                    $data["DepartmentId"]
                );
            } else {
                throw new Exception("Padalinys su ID " . $divisionId . " nerastas.");
            }
        } else {
            throw new Exception("Klaida vykdant SQL užklausą: " . $stmt->error);
        }

        $stmt->close();
        $conn->close();

        return $division;
    }

    // Gauna padalinio detales pagal ID
    public function getDivisionDetailsById($divisionId)
    {
        $conn = connectDB();
        $stmt = $conn->prepare("SELECT
            d.Id,
            d.DivisionName,
            d.DepartmentId,
            dp.DepartmentName
        FROM divisions d
            LEFT JOIN departments dp ON d.DepartmentId = dp.Id
        WHERE d.Id = ?");

        if (!$stmt) {
            throw new Exception("Klaida formuojant SQL užklausą: " . $conn->error);
        }

        $stmt->bind_param("i", $divisionId);

        $divisionDetails = null;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $data = $result->fetch_assoc();

            $divisionDetails = new DivisionDetails(
                $data["Id"],
                $data["DivisionName"],
                $data["DepartmentId"],
                $data["DepartmentName"]
            );
        } else {
            throw new Exception("Klaida vykdant SQL užklausą: " . $stmt->error);
        }

        $stmt->close();
        $conn->close();

        return $divisionDetails;
    }

    // Įterpia naują padalinį
    public function insertDivision($divname, $departId)
    {
        $conn = connectDB();
        // paruošiame mysql komandą
        $stmt = $conn->prepare("INSERT INTO divisions (DivisionName, DepartmentId) VALUES (?, ?);");

        // prijugiam/priskiriam reikšmes/parametrus
        $stmt->bind_param("si", $divname, $departId);

        // paleisim komanda
        if ($stmt->execute()) {
            // išsaugokite pranešimą sesijoje
            $_SESSION['message'] = "Duomenys įterpti!";
            return true;
        } else {
            throw new Exception("Error: " . $stmt->error);
        }
    }

    // Atnaujina padalinio informaciją
    public function updateDivision($id, $divisionName, $departmentId)
    {
        $conn = connectDB();

        $stmt = $conn->prepare("UPDATE divisions SET DivisionName = ?, DepartmentId = ? WHERE Id = ?");

        if (!$stmt) {
            throw new Exception("Klaida formuojant SQL užklausą: " . $conn->error);
        }

        $stmt->bind_param(
            "sii",
            $divisionName,
            $departmentId,
            $id
        );

        if (!$stmt->execute()) {
            throw new Exception("Klaida vykdant SQL užklausą: " . $stmt->error);
        }

        $stmt->close();
        $conn->close();

        return true;
    }
}
