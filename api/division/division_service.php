<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/division/Division.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/division/DivisionDetails.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/division/DivisionRepository.php";

function handleGetAllDivisionsRequest()
{
    // susikuriam repozitorijos objekta
    $repository = new DivisionRepository();

    // kvieciu repozitorijos metoda
    $result = $repository->getDivisions();

    // nustatomas header, pasakoma klientui, kad bus grazinamas json formatas
    header('Content-Type: application/json');

    http_response_code(200);
    echo json_encode($result);
}

function handleGetSingleDivisionRequest($id)
{
    // susikuriam repozitorijos objekta
    $repository = new DivisionRepository();

    // kvieciu repozitorijos metoda
    $result = $repository->getDivisionDetailsById($id);

    // nustatomas header, pasakoma klientui, kad bus grazinamas json formatas
    header('Content-Type: application/json');

    http_response_code(200);
    echo json_encode($result);
}

function handlePostRequest()
{
    $json = file_get_contents('php://input');
    $division = json_decode($json);

    // susikuriame repozitorijos objekta
    $repository = new DivisionRepository();

    // kvieciu repozitorijos metoda
    $result = $repository->insertDivision(
        $division->divisionName,
        $division->departmentId

    );

    // nustatomas header, pasakoma klientui, kad bus grazinamas json formatas
    header('Content-Type: application/json');

    // Send a response back to the client
    if ($result) {
        http_response_code(201); // 201 Created
        echo json_encode(array('message' => 'Division created successfully'));
    } else {
        http_response_code(500); // 500 Internal Server Error
        echo json_encode(array('message' => 'An error occurred while creating the division'));
    }
}

function handlePutRequest($id)
{
    // Get the input data
    $inputData = json_decode(file_get_contents('php://input'), true);

    // Create an Division object
    $division = new Division(
        $id,
        $inputData['divisionName'],
        $inputData['departmentId']

    );

    // Update the division in the database
    $repository = new DivisionRepository();
    $result = $repository->updateDivision($division->id, $division->divisionName, $division->departmentId);

    // nustatomas header, pasakoma klientui, kad bus grazinamas json formatas
    header('Content-Type: application/json');

    // Send the appropriate HTTP response
    if ($result) {
        http_response_code(200);
        echo json_encode(['message' => 'Division updated successfully']);
    } else {
        http_response_code(500);
        echo json_encode(['message' => 'An error occurred while updating the division']);
    }
}

function handleDeleteRequest()
{
    // nustatomas header, pasakoma klientui, kad bus grazinamas json formatas
    header('Content-Type: application/json');

    echo json_encode(['message' => 'Not supported']);
    http_response_code(200);
}
