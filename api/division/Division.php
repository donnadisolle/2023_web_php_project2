<?php

class Division
{
    public $id;
    public $divisionName;
    public $departmentId;

    public function __construct($id, $divisionName, $departmentId)
    {
        $this->id = $id;
        $this->divisionName = $divisionName;
        $this->departmentId = $departmentId;
    }
}
