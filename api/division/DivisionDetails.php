<?php

class DivisionDetails extends Division
{
    public $divisionName;
    public $departmentName;

    public function __construct($id, $divisionName, $departmentId, $departmentName)
    {
        parent::__construct($id, $divisionName, $departmentId);

        $this->divisionName = $divisionName;
        $this->departmentName = $departmentName;
    }
}
