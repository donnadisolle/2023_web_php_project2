<?php

function connectDB()
{
    $serverName = "localhost";
    $username = "root";
    $password = "";
    $dbname = "company_db";

    // sukursime prisijungimą
    // sukursime prisijungimo objektą, naudodami klasę mysqli
    $conn = new mysqli($serverName, $username, $password, $dbname);

    // patikrinsime prisijungimą
    if ($conn->connect_error) {
        // logika kai prisijungimas nepavyksta
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}

// $serverName = "MYSQL5048.site4now.net";
// $username = "a7ba3f_company";
// $password = "eSF24qFaCJ5BeAv";
// $dbname = "db_a7ba3f_company";

// $serverName = "localhost";
// $username = "root";
// $password = "";
// $dbname = "company";

// $serverName = "localhost";
// $username = "root";
// $password = "";
// $dbname = "company_db";
