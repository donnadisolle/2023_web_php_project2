<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/db_connect.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/user/User.php";

class UserRepository
{
    public function insertUser($user)
    {
        $conn = connectDB();

        $stmt = $conn->prepare("INSERT INTO users (Name, Surname, Email, Password, Enabled) VALUES (?, ?, ?, ?, ?)");

        if (!$stmt) {
            throw new Exception("SQL query error: " . $conn->error);
        }

        $name = $user->getName();
        $surname = $user->getSurname();
        $email = $user->getEmail();
        $password = $user->getPassword();
        $enabled = $user->getEnabled();

        $stmt->bind_param("ssssi", $name, $surname, $email, $password, $enabled);

        if (!$stmt->execute()) {
            throw new Exception("SQL query execution error: " . $stmt->error);
        }

        $stmt->close();
        $conn->close();

        return true;
    }

    public function getUserByEmail($email)
    {
        $conn = connectDB();

        $stmt = $conn->prepare("SELECT * FROM users WHERE Enabled = 1 && Email = ?");

        if (!$stmt) {
            throw new Exception("SQL query error: " . $conn->error);
        }

        $stmt->bind_param("s", $email);

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $data = $result->fetch_assoc();

            if ($data) {
                $user = new User();
                $user->setId($data["Id"]);
                $user->setName($data["Name"]);
                $user->setSurname($data["Surname"]);
                $user->setEmail($data["Email"]);
                $user->setPassword($data["Password"]);
                $user->setEnabled($data["Enabled"]);

                $stmt->close();
                $conn->close();

                return $user;
            }
        } else {
            throw new Exception("SQL query execution error: " . $stmt->error);
        }

        $stmt->close();
        $conn->close();

        return null;
    }
}
