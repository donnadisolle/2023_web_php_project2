<?php
session_start();

// If the user is not logged in, redirect to the login page.
if (!isset($_SESSION['userId'])) {
    header('Location: pages/login.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/pages/parts/logged_in_nav.php";
?>

    <div class="container">
        <h1>Welcome, <?php echo htmlspecialchars($_SESSION['userName']); ?>!</h1>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>