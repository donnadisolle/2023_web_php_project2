<?php
session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . "/api/user/UserRepository.php";

$email = $_POST['email'];
$password = $_POST['password'];

$userRepository = new UserRepository();
$user = $userRepository->getUserByEmail($email);

if ($user !== null && password_verify($password, $user->getPassword())) {
    // Password is correct. Login the user.
    $_SESSION['userId'] = $user->getId();
    $_SESSION['userName'] = $user->getName();

    // Redirect to some protected page.
    header('Location: ../../index.php');
    exit;
} else {
    echo "asdasdasdasasdzasxdfcadsFRGRTEY4W";
    // Invalid credentials. Show an error message.
    $_SESSION['error'] = 'Invalid email or password or account is not enabled';
    // Redirect back to login page.
    header('Location: ../login.php');
    exit;
}
