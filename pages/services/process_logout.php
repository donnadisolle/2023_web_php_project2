<?php
session_start();

// Destroy and unset all of the session variables.
$_SESSION = array();
session_destroy();

// Redirect to the login page.
header('Location: /pages/login.php');
exit;
