<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/api/user/User.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/user/UserRepository.php";

// Retrieve form data
$name = $_POST['registerName'];
$surname = $_POST['registerSurname'];
$email = $_POST['registerEmail'];
$password = $_POST['registerPassword'];

// Hash the password - IMPORTANT!
$hashedPassword = password_hash($password, PASSWORD_DEFAULT);

// Create a new User object
$user = new User();
$user->setName($name);
$user->setSurname($surname);
$user->setEmail($email);
$user->setPassword($hashedPassword);
$user->setEnabled(0);

// Insert the new User into the database
$repository = new UserRepository();
$result = $repository->insertUser($user);

if ($result) {
    // User was successfully inserted - redirect or output success message
    header('Location: ../login.php');
} else {
    // Something went wrong - output an error message
    echo "Registration failed. Please try again.";
}
