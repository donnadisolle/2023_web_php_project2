<?php

// If the user is not logged in, redirect to the login page.
if (!isset($_SESSION['userId'])) {
    header('Location: /pages/login.php');
    exit;
}
