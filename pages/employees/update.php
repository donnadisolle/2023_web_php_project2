<?php
session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . "/api/employee/Employee.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/employee/EmployeeRepository.php";
$employeeRepository = new EmployeeRepository();

$employee = null;
if (isset($_GET["employeeId"])) {
    try {
        $employeeId = htmlspecialchars($_GET["employeeId"]);
        $employee = $employeeRepository->getEmployee($employeeId);
    } catch (Exception $e) {

        $_SESSION['error'] = "Klaida: " . $e->getMessage();
        header('Location: update.php?employeeId=' . $id);
    }

}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    processPost();
}

function processPost()
{
    global $employeeRepository;
    if (isValidForm()) {
        $id = htmlspecialchars($_POST["id"]);
        $name = htmlspecialchars($_POST["name"]);
        $surname = htmlspecialchars($_POST["surname"]);
        $dateBirth = htmlspecialchars($_POST["dateBirth"]);
        $divId = htmlspecialchars($_POST["divId"]);
        $docNumber = htmlspecialchars($_POST["docNumber"]);
        $midName = htmlspecialchars($_POST["midName"]);
        $salary = htmlspecialchars($_POST["salary"]);

        try {
            if ($employeeRepository->updateEmployee($id, $name, $surname, $dateBirth, $divId, $docNumber, $midName, $salary)) {
                $_SESSION['message'] = "Duomenys atnaujinti!";
                header('Location: list.php');
                exit;
            } else {
                throw new Exception("Error: Update failed.");
            }
        } catch (Exception $e) {
            $_SESSION['error'] = "Klaida: " . $e->getMessage();
            header('Location: list.php');
        }
    }
}

function isValidForm()
{
    // Same validation rules as before
    return isset($_POST["name"]) && $_POST["name"] != ""
    && isset($_POST["surname"]) && $_POST["surname"] != ""
    && isset($_POST["dateBirth"]) && $_POST["dateBirth"] != ""
    && isset($_POST["divId"]) && $_POST["divId"] != 0
    && isset($_POST["midName"])
    && isset($_POST["salary"]) && $_POST["salary"] != 0
    && isset($_POST["docNumber"]) && $_POST["docNumber"] != ""
    && isset($_POST["id"]);
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Įrašo forma</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/pages/parts/logged_in_nav.php";
?>
    <div class="container">
        <h1>Įrašo forma</h1>
        <a href="list.php" class="btn btn-primary mb-2">Grįžti į sąrašą</a>
        <form action="<?='update.php?employeeId=' . htmlspecialchars($employee->id)?>" method="post">
            <input type="hidden" name="id" value="<?=htmlspecialchars($employee->id)?>">
            <div class="form-group">
                <label for="name">Vardas:</label>
                <input type="text" class="form-control" id="name" name="name" value="<?=htmlspecialchars($employee->firstName)?>" required>
            </div>
            <div class="form-group">
                <label for="surname">Pavardė:</label>
                <input type="text" class="form-control" id="surname" name="surname" value="<?=htmlspecialchars($employee->lastName)?>" required>
            </div>
            <div class="form-group">
                <label for="dateBirth">Gimimo data:</label>
                <input type="date" class="form-control" id="dateBirth" name="dateBirth" value="<?=htmlspecialchars($employee->birthDay)?>" required>
            </div>
            <div class="form-group">
                <label for="divId">Skyriaus ID:</label>
                <input type="number" class="form-control" id="divId" name="divId" value="<?=htmlspecialchars($employee->divisionId)?>">
            </div>
            <div class="form-group">
                <label for="docNumber">Dokumento numeris:</label>
                <input type="text" class="form-control" id="docNumber" name="docNumber" value="<?=htmlspecialchars($employee->documentNumber)?>" required>
            </div>
            <div class="form-group">
                <label for="midName">Vidurinės dalies vardas:</label>
                <input type="text" class="form-control" id="midName" name="midName" value="<?=htmlspecialchars($employee->middleName)?>">
            </div>
            <div class="form-group">
                <label for="salary">Atlyginimas:</label>
                <input type="number" class="form-control" id="salary" name="salary" value="<?=htmlspecialchars($employee->salary)?>" required>
            </div>
            <button type="submit" class="btn btn-primary">Pateikti</button>
        </form>
    </div>
</body>
</html>