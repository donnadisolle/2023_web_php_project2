<?php
session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . "/api/employee/EmployeeRepository.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/department/DepartmentRepository.php";

$employee = null;
$isManager = false;
$managerDepartmentName = "";

try {
    if (!isset($_GET["employeeId"]) || empty($_GET["employeeId"])) {
        throw new Exception("No employee ID provided");
    }

    $employeeId = $_GET["employeeId"];

    $employeeRepository = new EmployeeRepository();
    $employee = $employeeRepository->getEmployeeDetailsById($employeeId);

    $departmentRepository = new DepartmentRepository();
    $managerDepartment = $departmentRepository->getDepartmentByManagerId($employeeId);

    if ($managerDepartment) {
        $isManager = true;
        $managerDepartmentName = $managerDepartment->departmentName;
    }

} catch (Exception $e) {
    $_SESSION['error'] = "Error: " . $e->getMessage();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Employee Information</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/pages/parts/logged_in_nav.php";
?>
    <div class="container">
        <h1>Employee Information</h1>
        <a href="list.php" class="btn btn-primary mb-2">Back to List</a>

        <!-- Display error message -->
        <?php if (isset($_SESSION['error'])): ?>
            <div class="alert alert-danger">
                <?php echo $_SESSION['error'];unset($_SESSION['error']); ?>
            </div>
        <?php endif;?>

        <?php if ($employee): ?>
            <div class='card' style='width: 18rem;'>
                <div class='card-body'>
                    <h5 class='card-title'>
                        <?php echo htmlspecialchars($employee->firstName) . " " . htmlspecialchars($employee->lastName) . ($isManager ? " (Manager)" : ""); ?>
                    </h5>
                    <h6 class='card-subtitle mb-2 text-muted'>Birth Date: <?php echo htmlspecialchars($employee->birthDay); ?></h6>
                    <p class='card-text'>Salary: <?php echo htmlspecialchars($employee->salary); ?></p>
                    <p class='card-text'>Division: <?php echo htmlspecialchars($employee->divisionName); ?></p>
                    <p class='card-text'>Department: <?php echo htmlspecialchars(($isManager ? $managerDepartmentName : $employee->departmentName)); ?></p>
                </div>
            </div>
        <?php else: ?>
            <p>Employee information not found</p>
        <?php endif;?>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>