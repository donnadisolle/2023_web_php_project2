<?php
session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . "/api/employee/Employee.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/api/employee/EmployeeRepository.php";

// show successful message after action
if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
    echo "<div class='alert alert-success'>$message</div>";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/pages/parts/logged_in_nav.php";
?>
    <div class="container">
        <h1>Darbuotojų sąrašas</h1>

        <!-- Display error message -->
        <?php if (isset($_SESSION['error'])): ?>
            <div class="alert alert-danger">
                <?php echo $_SESSION['error'];unset($_SESSION['error']); ?>
            </div>
        <?php endif;?>

        <a href="insert.php" class="btn btn-success mb-2">Create New Employee</a> <!-- Added Create button here -->

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Vardas</th>
                    <th>Pavardė</th>
                    <th>GimimoData</th>
                    <th>Atlyginimas</th>
                    <th>Veiksmai</th>
                </tr>
            </thead>
            <tbody>

<?php

try {
    $repository = new EmployeeRepository();

    $employees = $repository->getEmployees();
    if (count($employees) > 0) {
        foreach ($employees as $employee) {
            echo "<tr><td>" . htmlspecialchars($employee->id) . "</td><td>" . htmlspecialchars($employee->firstName) . "</td><td>" . htmlspecialchars($employee->lastName) . "</td><td>" . htmlspecialchars($employee->birthDay) . "</td><td>" . htmlspecialchars($employee->salary) . "</td><td><a href='update.php?employeeId=" . htmlspecialchars($employee->id) . "' class='btn btn-primary'>Redaguoti</a> <a href='get.php?employeeId=" . htmlspecialchars($employee->id) . "' class='btn btn-info'>Peržiūrėti</a></td></tr>";
        }
    } else {
        echo "<tr><td colspan='6'>Nėra darbuotojų</td></tr>";
    }
} catch (Exception $e) {
    $_SESSION['error'] = "Klaida: " . $e->getMessage();
}
?>

            </tbody>
        </table>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>