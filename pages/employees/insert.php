<?php
session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . "/api/employee/EmployeeRepository.php";

?>

<!DOCTYPE html>
<html>
<head>
    <title>Įrašo forma</title>
    <!-- Įterpkite Bootstrap CSS nuorodą -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/pages/parts/logged_in_nav.php";
?>
    <div class="container">
        <h1>Įrašo forma</h1>

        <!-- Display error message -->
        <?php if (isset($_SESSION['error'])): ?>
            <div class="alert alert-danger">
                <?php echo $_SESSION['error'];unset($_SESSION['error']); ?>
            </div>
        <?php endif;?>

        <a href="list.php" class="btn btn-primary mb-2">Grįžti į sąrašą</a>
        <form action="insert.php" method="post">
            <div class="form-group">
                <label for="name">Vardas:</label>
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="form-group">
                <label for="surname">Pavardė:</label>
                <input type="text" class="form-control" id="surname" name="surname" required>
            </div>
            <div class="form-group">
                <label for="dateBirth">Gimimo data:</label>
                <input type="date" class="form-control" id="dateBirth" name="dateBirth" required>
            </div>
            <div class="form-group">
                <label for="divId">Skyriaus ID:</label>
                <input type="number" class="form-control" id="divId" name="divId" required>
            </div>
            <div class="form-group">
                <label for="docNumber">Dokumento numeris:</label>
                <input type="text" class="form-control" id="docNumber" name="docNumber" required>
            </div>
            <div class="form-group">
                <label for="midName">Vidurinės dalies vardas:</label>
                <input type="text" class="form-control" id="midName" name="midName">
            </div>
            <div class="form-group">
                <label for="salary">Atlyginimas:</label>
                <input type="number" class="form-control" id="salary" name="salary" required>
            </div>
            <button type="submit" class="btn btn-primary">Pateikti</button>
        </form>
    </div>
</body>
</html>

<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    processPost();
}

function processPost()
{
    if (isValidForm()) {
        $name = htmlspecialchars($_POST["name"]);
        $surname = htmlspecialchars($_POST["surname"]);
        $dateBirth = htmlspecialchars($_POST["dateBirth"]);
        $divId = htmlspecialchars($_POST["divId"]);
        $docNumber = htmlspecialchars($_POST["docNumber"]);
        $midName = htmlspecialchars($_POST["midName"]);
        $salary = htmlspecialchars($_POST["salary"]);

        $employeeRepository = new EmployeeRepository();

        try {
            if ($employeeRepository->insertEmployee($name, $surname, $dateBirth, $divId, $docNumber, $midName, $salary)) {
                // Nukreipkite į list.php puslapį
                header('Location: list.php');
                exit;
            }
        } catch (Exception $e) {
            $_SESSION['error'] = "Klaida: " . $e->getMessage();
            header('Location: insert.php');
        }
    }
}

function isValidForm()
{
    return isset($_POST["name"]) && $_POST["name"] != ""
    && isset($_POST["surname"]) && $_POST["surname"] != ""
    && isset($_POST["dateBirth"]) && $_POST["dateBirth"] != ""
    && isset($_POST["divId"]) && $_POST["divId"] != 0
    && isset($_POST["midName"])
    && isset($_POST["salary"]) && $_POST["salary"] != 0
    && isset($_POST["docNumber"]) && $_POST["docNumber"] != "";
}