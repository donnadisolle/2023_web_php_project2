<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Dashboard</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="pages/employees/list.php">Employees</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="pages/divisions/list.php">Divisions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="pages/departments/list.php">Departments</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/pages/services/process_logout.php">Logout</a>
                </li>
            </ul>
        </div>
    </nav>
