<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h3>Register</h3>
        <form action="services/process_register.php" method="post">
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="registerName" required>
            </div>
            <div class="form-group">
                <label>Surname</label>
                <input type="text" class="form-control" name="registerSurname" required>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name="registerEmail" required>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="registerPassword" required>
            </div>
            <button type="submit" class="btn btn-success">Register</button>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
